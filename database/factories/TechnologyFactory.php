<?php

namespace Database\Factories;

use App\Models\Page;
use App\Models\Technology;
use Illuminate\Database\Eloquent\Factories\Factory;

class TechnologyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Technology::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'link' => $this->faker->url(),
            'icon' => $this->faker->word,
            'title' => $this->faker->word,
            'favourite' => $this->faker->boolean,
            'sort_order' => $this->faker->randomDigitNotNull(),
        ];
    }

}
