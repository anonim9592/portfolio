<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    public function run()
    {
        $data = [];
        $data[] = [
            'title' => 'Ronin Athletics',
            'description' => 'Ronin Athletics - is a Martial arts gym in New York',
            'link' => 'https://roninathletics.com/',
//            'image' => null,
        ];
        $data[] = [
            'title' => 'Admiral Bureau',
            'description' => 'Professional translation service',
            'link' => 'https://admiral.com.ua/',
//            'image' => null,
        ];
        $data[] = [
            'title' => 'Rafael`s Barbershop',
            'description' => 'The best barber shop in Manhattan',
            'link' => 'https://rafaelsbarbershop.com/',
//            'image' => null,
        ];
        $data[] = [
            'title' => 'Rafael`s Barbershop',
            'description' => 'The best barber shop in Manhattan',
            'link' => 'https://rafaelsbarbershop.com/',
//            'image' => null,
        ];
        $data[] = [
            'title' => 'TonTop',
            'description' => 'Women`s clothes store',
            'link' => 'https://tontop.com.ua/',
//            'image' => null,
        ];
        $data[] = [
            'title' => 'Zolotaya',
            'description' => 'Women`s clothes store',
            'link' => 'https://zolotaya.net/',
//            'image' => null,
        ];
        $data[] = [
            'title' => 'Technoguma',
            'description' => 'Rubber products store',
            'link' => 'https://tehnoguma.com.ua/',
//            'image' => null,
        ];
        foreach ($data as $page) {
            Project::factory()->create($page);
        }
    }
}
