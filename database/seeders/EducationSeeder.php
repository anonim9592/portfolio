<?php

namespace Database\Seeders;

use App\Models\Education;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class EducationSeeder extends Seeder
{
    public function run()
    {
        $data = [];
        $data[] = [
            'title' => 'Khmelnytskyi National University',
            'description' => 'Master of Engineering (M.Eng.)',
            'school' => 'Khmelnytskyi National University',
            'speciality' => 'Computer Engineering',
            'from' => Carbon::create(2012),
            'to' => Carbon::create(2017),
        ];
        foreach ($data as $page) {
            Education::factory()->create($page);
        }
    }
}
