<?php

namespace Database\Seeders;

use App\Models\Technology;
use Illuminate\Database\Seeder;

class TechnologySeeder extends Seeder
{
    public function run()
    {
        $data = [];
        $data[] = [
            'link' => 'https://www.php.net/',
            'icon' => 'icon-php',
            'title' => 'PHP',
            'favourite' => true,
            'sort_order' => 0,
        ];
        $data[] = [
            'link' => 'https://laravel.com/',
            'icon' => 'icon-laravel-alt',
            'title' => 'Laravel',
            'favourite' => true,
            'sort_order' => 1,
        ];
        $data[] = [
            'link' => 'https://devdocs.magento.com/',
            'icon' => 'icon-magento',
            'title' => 'Magento 2',
            'favourite' => false,
            'sort_order' => 2,
        ];
        $data[] = [
            'link' => 'https://www.yiiframework.com/',
            'icon' => 'icon-opensource',
            'title' => 'Yii2',
            'favourite' => false,
            'sort_order' => 3,
        ];
        $data[] = [
            'link' => 'https://devdocs.magento.com/',
            'icon' => 'icon-magento',
            'title' => 'Magento 2',
            'favourite' => false,
            'sort_order' => 4,
        ];
        $data[] = [
            'link' => 'https://www.opencart.com/',
            'icon' => 'icon-opensource',
            'title' => 'OpenCart',
            'favourite' => false,
            'sort_order' => 5,
        ];
        $data[] = [
            'link' => 'https://www.postgresql.org/',
            'icon' => 'icon-postgres',
            'title' => 'Postgres',
            'favourite' => true,
            'sort_order' => 6,
        ];
        $data[] = [
            'link' => 'https://www.mysql.com/',
            'icon' => 'icon-mysql',
            'title' => 'MySQL',
            'favourite' => false,
        ];
        $data[] = [
            'link' => 'http://vanilla-js.com/',
            'icon' => 'icon-js',
            'title' => 'Vanilla Js',
            'favourite' => false,
        ];
        $data[] = [
            'link' => 'https://jquery.com/',
            'icon' => 'icon-jquery',
            'title' => 'Jquery',
            'favourite' => false,
        ];

        $data[] = [
            'link' => 'https://vuejs.org/',
            'icon' => 'icon-js',
            'title' => 'VueJs',
            'favourite' => true,
        ];
        $data[] = [
            'link' => 'https://en.wikipedia.org/wiki/Representational_state_transfer',
            'icon' => 'icon-opensource',
            'title' => 'REST API',
            'favourite' => true,
        ];
        $data[] = [
            'link' => 'https://en.wikipedia.org/wiki/SOAP',
            'icon' => 'icon-opensource',
            'title' => 'SOAP API',
            'favourite' => false,
        ];
        $data[] = [
            'link' => 'https://www.docker.com/',
            'icon' => 'icon-opensource',
            'title' => 'Docker',
            'favourite' => true,
        ];
        $data[] = [
            'link' => 'https://nginx.org/en/',
            'icon' => 'icon-nginx',
            'title' => 'Nginx',
            'favourite' => true,
        ];
        $data[] = [
            'link' => 'https://www.elastic.co/',
            'icon' => 'icon-opensource',
            'title' => 'Elasticsearch',
            'favourite' => false,
        ];
        $data[] = [
            'link' => 'https://www.rabbitmq.com/',
            'icon' => 'icon-opensource',
            'title' => 'RabbitMQ',
            'favourite' => false,
        ];
        $data[] = [
            'link' => 'https://www.rabbitmq.com/',
            'icon' => 'icon-opensource',
            'title' => 'RabbitMQ',
            'favourite' => false,
        ];
        $data[] = [
            'link' => 'https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/CSS_basics',
            'icon' => 'icon-css3',
            'title' => 'CSS 3',
            'favourite' => false,
        ];
        $data[] = [
            'link' => 'https://developer.mozilla.org/en-US/docs/Web/HTML',
            'icon' => 'icon-html5',
            'title' => 'HTML 5',
            'favourite' => false,
        ];
        $data[] = [
            'link' => 'https://git-scm.com/',
            'icon' => 'icon-git',
            'title' => 'Git (github, gitlab, bitbucket)',
            'favourite' => true,
        ];

        foreach ($data as $page) {
            Technology::factory()->create($page);
        }
    }
}
