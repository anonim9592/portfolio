<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    public function run()
    {
        $data = [];
        $data[] = [
            'slug' => 'index',
            'title' => 'Home',
        ];
        $data[] = [
            'slug' => 'resume',
            'title' => 'Resume',
        ];
        $data[] = [
            'slug' => 'portfolio',
            'title' => 'Portfolio',
        ];
        $data[] = [
            'slug' => 'contact',
            'title' => 'Contact',
        ];
        foreach ($data as $page) {
            Page::factory()->create($page);
        }
    }
}
