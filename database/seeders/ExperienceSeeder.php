<?php

namespace Database\Seeders;

use App\Models\Experience;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ExperienceSeeder extends Seeder
{
    public function run()
    {
        $data = [];
        $data[] = [
            'title' => 'Magento 2 Back-end Developer',
            'description' => '
                    <ul>
                        <li>Developing and maintaining back-end side of projects using Magento 2</li>
                        <li>External APIs integration</li>
                        <li>Development of custom modules</li>
                        <li>Refactoring and of existing projects</li>
                    </ul>
                ',
            'company' => 'Elogic commerce',
            'position' => 'Back-end Developer',
            'from' => Carbon::create(2021, 9),
            'to' => null,
        ];
        $data[] = [
            'title' => 'Laravel Back-end Developer',
            'description' => '
                    <ul>
                        <li>Developing and maintaining back-end side of projects using Laravel</li>
                        <li>External APIs integration</li>
                        <li>Unit tests integration</li>
                        <li>Refactoring of existing projects</li>
                    </ul>
                ',
            'company' => 'MassMedia group',
            'position' => 'Back-end Developer',
            'from' => Carbon::create(2021, 3),
            'to' => Carbon::create(2021, 9),
        ];
        $data[] = [
            'title' => 'Back-end Developer',
            'description' => '
                    <ul>
                        <li>Developing and maintaining projects using Laravel, Yii2, sometimes VueJs</li>
                        <li>New features integration</li>
                        <li>Integration with external APIs</li>
                        <li>Refactoring of legacy code</li>
                        <li>Refactoring of existing projects</li>
                        <li>SEO features integration (meta-tags, speed optimisation, integration with tag-managers)</li>
                    </ul>
                ',
            'company' => 'Imrev',
            'position' => 'Back-end Developer',
            'from' => Carbon::create(2019, 3),
            'to' => Carbon::create(2021, 3),
        ];
        $data[] = [
            'title' => 'OpenCart Developer',
            'description' => '
                    <ul>
                        <li>Developing and maintaining projects using OpenCart</li>
                        <li>New features integration</li>
                    </ul>
                ',
            'company' => 'GalaxyIT',
            'position' => 'Back-end Developer',
            'from' => Carbon::create(2018, 9),
            'to' => Carbon::create(2019, 1),
        ];
        $data[] = [
            'title' => 'Full-stack Web Developer',
            'description' => '
                    <ul>
                        <li>Developing and maintaining projects</li>
                        <li>Implementation of front-end logic using jQuery, JS</li>
                        <li>Implementation of back-end logic using OpenCart, WordPress, Ruby on Rails, ModX and a lot of other technologies =)</li>
                    </ul>
                ',
            'company' => 'ItSites',
            'position' => 'Front-end Developer/Opencart Developer/Ruby on Rails Developer',
            'from' => Carbon::create(2017, 5),
            'to' => Carbon::create(2018, 9),
        ];
        foreach ($data as $page) {
            Experience::factory()->create($page);
        }
    }
}
