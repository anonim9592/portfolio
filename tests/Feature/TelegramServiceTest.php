<?php

namespace Tests\Feature;

use App\DTO\ContactFormDTO;
use App\Service\TelegramService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TelegramServiceTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_send_message()
    {
        $service = $this->getService();
        $data = [
            'name' => 'test',
            'email' => $this->faker->email,
            'message' => 'test message'
        ];
        $formDto = new ContactFormDTO();
        $formDto->fillFromArray($data);

        $result = $service->sendMessageToBot($formDto);
        $this->assertStringContainsString($formDto->getMessage(), $result->text);
    }

    private function getService(): TelegramService
    {
        return resolve(TelegramService::class);
    }
}
