<?php

namespace Tests\Feature;

use App\Models\ContactForm;
use App\Service\ContactService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactServiceTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_create_contact_form()
    {
        $service = $this->getService();
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'message' => $this->faker->sentence(15)
        ];

        $result = $service->fillAndSave($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas($this->getTableName(), $data);

    }

    public function test_create_contact_form_without_name()
    {
        $service = $this->getService();
        $data = [
            'email' => $this->faker->email,
            'message' => $this->faker->sentence(15)
        ];

        $result = $service->fillAndSave($data);

        $this->assertFalse($result);

    }

    private function getService(): ContactService
    {
        return resolve(ContactService::class);
    }

    private function getTableName(): string
    {
        return ContactForm::query()->getQuery()->from;
    }
}
