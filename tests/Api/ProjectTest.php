<?php

namespace Tests\Api;

use App\Models\Project;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_send_request()
    {
        Project::factory()->create(['title' => $title = $this->faker->word]);

        $response = $this->post(route('project.list'), []);

        $response->assertJsonStructure([
            'success',
            'result' => [
                [
                    'title',
                    'description',
                    'link',
                    'image'
                ]
            ],
            'message']);
        $response->assertJsonFragment(['title' => $title]);
    }
}
