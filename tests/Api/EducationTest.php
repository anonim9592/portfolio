<?php

namespace Tests\Api;

use App\Models\Education;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EducationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_send_request()
    {
        Education::factory()->create(['title' => $title = $this->faker->word]);

        $response = $this->post(route('education.list'), []);

        $response->assertJsonStructure([
            'success',
            'result' => [
                [
                    'title',
                    'description',
                    'school',
                    'speciality',
                    'from',
                    'to'
                ]
            ],
            'message']);
        $response->assertJsonFragment(['title' => $title]);
    }
}
