<?php

namespace Tests\Api;

use App\Models\Page;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PagesTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_list()
    {
        Page::factory()->create(['title' => $title = 'test_title']);

        $response = $this->post(route('page.list'), []);

        $response->assertJsonStructure([
            'success',
            'result' => [
                [
                    'route', 'title'
                ]
            ],
            'message'
        ]);
        $response->assertJsonFragment(['title' => $title]);
    }
}
