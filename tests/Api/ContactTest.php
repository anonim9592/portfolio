<?php

namespace Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_send_request()
    {
        $data = [
            'name' => 'api test',
            'email' => $this->faker->email,
            'message' => 'test message'
        ];

        $response = $this->post(route('contact.submit'), $data);

        $response->assertStatus(201);
    }
}
