<?php

namespace Tests\Api;

use Tests\TestCase;

class LocaleTest extends TestCase
{
    public function test_get_locale()
    {
        $locale = \App::getLocale();

        $response = $this->post(route('locale.get'), []);

        $response->assertJsonStructure([
            'success',
            'result',
            'message'
        ]);
        $response->assertJsonFragment(['result' => $locale]);
    }

    public function test_change_locale()
    {
        $locale = \App::getLocale();
        $anotherLocale = $locale === 'en' ? 'uk' : 'en';

        $response = $this->post(route('locale.set'), ['locale' => $anotherLocale]);

        $response->assertJsonStructure([
            'success',
            'result',
            'message'
        ]);
        $response->assertJsonFragment(['result' => $anotherLocale]);
    }

    public function test_get_locale_list()
    {
        $list = config('laravellocalization.supportedLocales');

        $response = $this->post(route('locale.list'), []);

        $response->assertJsonStructure([
            'success',
            'result',
            'message'
        ]);
        $response->assertJsonFragment(['result' => $list]);
    }
}
