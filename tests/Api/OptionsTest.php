<?php

namespace Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OptionsTest extends TestCase
{

    public function test_get_options_by_list_of_keys()
    {
        $key1 = 'test_1';
        $key2 = 'test_2';

        $response = $this->post(route('translations.list'), ['keys' => [$key1, $key2]]);

        $response->assertJsonStructure([
            'success',
            'result' => [
                [
                    'key',
                    'value'
                ]
            ],
            'message'
        ]);
        $response->assertJsonFragment(['key' => $key1, 'value' => $key1]);
    }
}
