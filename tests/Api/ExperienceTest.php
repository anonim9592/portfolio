<?php

namespace Tests\Api;

use App\Models\Experience;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ExperienceTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_send_request()
    {
        Experience::factory()->create(['title' => $title = $this->faker->word]);

        $response = $this->post(route('experience.list'), []);

        $response->assertJsonStructure([
            'success',
            'result' => [
                [
                    'title',
                    'description',
                    'company',
                    'position',
                    'from',
                    'to'
                ]
            ],
            'message']);
        $response->assertJsonFragment(['title' => $title]);
    }
}
