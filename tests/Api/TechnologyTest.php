<?php

namespace Tests\Api;

use App\Models\Technology;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TechnologyTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_send_request()
    {
        Technology::factory()->create(['title' => $title = $this->faker->word]);

        $response = $this->post(route('technology.list'), []);

        $response->assertJsonStructure([
            'success',
            'result' => [
                [
                    'title', 'link', 'icon', 'favourite'
                ]
            ],
            'message']);
        $response->assertJsonFragment(['title' => $title]);
    }
}
