<?php

namespace App\Traits;

use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations as BaseHasTranslations;

trait HasTranslations
{
    use BaseHasTranslations;

    public function toArray()
    {
        $attributes = parent::toArray();
        foreach ($this->getTranslatableAttributes() as $field) {
            if (in_array($field, array_keys($attributes))) {
                $attributes[$field] = $this->getTranslation($field, \App::getLocale());
            }
        }
        return $attributes;
    }
}
