<?php

namespace App\Query;

use App\Models\Technology;
use Illuminate\Support\Collection;
use App\Interfaces\Data\Technology as TechnologyInterface;

class TechnologyQuery
{
    public function getList(): Collection
    {
        return Technology::select([
            TechnologyInterface::TITLE,
            TechnologyInterface::LINK,
            TechnologyInterface::ICON,
            TechnologyInterface::SORT_ORDER,
            TechnologyInterface::FAVOURITE
        ])
            ->orderBy(TechnologyInterface::SORT_ORDER)
            ->get();
    }

}
