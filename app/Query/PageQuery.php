<?php

namespace App\Query;

use App\Models\Page;
use Illuminate\Support\Collection;
use App\Interfaces\Data\Page as PageInterface;

class PageQuery
{
    public function getForRouterPages(): Collection
    {
        return Page::select([PageInterface::TITLE, PageInterface::SLUG])->get();
    }

    public function getBySlug(string $slug): Page
    {
        return Page::select([PageInterface::TITLE, PageInterface::SLUG, PageInterface::CONTENT])
            ->where([PageInterface::SLUG => $slug])
            ->first();
    }
}
