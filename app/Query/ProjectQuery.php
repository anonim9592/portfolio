<?php

namespace App\Query;

use App\Models\Project;
use Illuminate\Support\Collection;
use App\Interfaces\Data\Project as ProjectInterface;

class ProjectQuery
{
    public function getList(): Collection
    {
        return Project::select([
            ProjectInterface::TITLE,
            ProjectInterface::DESCRIPTION,
            ProjectInterface::LINK,
            ProjectInterface::IMAGE
        ])->get();
    }
}
