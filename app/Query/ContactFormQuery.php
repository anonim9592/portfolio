<?php

namespace App\Query;

use App\DTO\ContactFormDTO;
use App\Models\ContactForm;
use App\Interfaces\Data\ContactForm as ContactFormInterface;

class ContactFormQuery
{
    public function saveToDb(ContactFormDTO $formDTO): bool
    {
        $form = new ContactForm();

        $form->fill([
            ContactFormInterface::NAME => $formDTO->getName(),
            ContactFormInterface::EMAIL => $formDTO->getEmail(),
            ContactFormInterface::MESSAGE => $formDTO->getMessage(),
        ]);

        return $form->save();
    }
}
