<?php

namespace App\Query;

use App\Models\Option;
use Illuminate\Database\Eloquent\Collection;
use App\Interfaces\Data\Option as OptionInterface;

class OptionQuery
{
    public function exists($key): bool
    {
        return Option::where(OptionInterface::KEY, '=', $key)->exists();
    }

    public function get($key, $default = null): ?string
    {
        if ($option = Option::where(OptionInterface::KEY, $key)->first()) {
            return $option->value;
        }

        return $default;
    }

    public function getMany($keys): ?Collection
    {
        $options = Option::select([OptionInterface::KEY, OptionInterface::VALUE])->whereIn(OptionInterface::KEY, $keys)->get();

        return $options;
    }

    public function set($key, $value = null): string
    {
        Option::updateOrCreate([OptionInterface::KEY => $key], [OptionInterface::VALUE => $value]);

        return $this->get($key);
    }

    public function remove($key): bool
    {
        return (bool)Option::where(OptionInterface::KEY, $key)->delete();
    }
}
