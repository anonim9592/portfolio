<?php

namespace App\Query;

use App\Models\Experience;
use Illuminate\Support\Collection;
use App\Interfaces\Data\Experience as ExperienceInterface;

class ExperienceQuery
{
    public function getList(): Collection
    {
        return Experience::select([
            ExperienceInterface::TITLE,
            ExperienceInterface::DESCRIPTION,
            ExperienceInterface::COMPANY,
            ExperienceInterface::POSITION,
            ExperienceInterface::FROM,
            ExperienceInterface::TO
        ])
            ->orderByDesc(ExperienceInterface::FROM)
            ->get();
    }

}
