<?php

namespace App\Query;

use App\Models\Education;
use Illuminate\Support\Collection;
use App\Interfaces\Data\Education as EducationInterface;

class EducationQuery
{
    public function getList(): Collection
    {
        return Education::select([
            EducationInterface::TITLE,
            EducationInterface::DESCRIPTION,
            EducationInterface::SCHOOL,
            EducationInterface::SPECIALITY,
            EducationInterface::FROM,
            EducationInterface::TO,
        ])
            ->get();
    }

}
