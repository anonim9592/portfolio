<?php

namespace App\DTO;

class ContactFormDTO
{
    private string $name;
    private string $email;
    private string $message;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): ContactFormDTO
    {
        $this->name = $name;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): ContactFormDTO
    {
        $this->email = $email;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): ContactFormDTO
    {
        $this->message = $message;
        return $this;
    }

    public function fillFromArray (array $formData): ContactFormDTO
    {
        $this->setName($formData['name'])
            ->setEmail($formData['email'])
            ->setMessage($formData['message']);

        return $this;
    }

}
