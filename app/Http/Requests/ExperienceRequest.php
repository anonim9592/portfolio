<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Interfaces\Data\Experience;

class ExperienceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Experience::TITLE => ['required', 'string', 'max:255'],
            Experience::DESCRIPTION => ['required', 'string', 'max:1000'],
            Experience::COMPANY => ['required', 'string', 'max:255'],
            Experience::POSITION => ['required', 'string', 'max:255'],
            Experience::FROM => ['required', 'date'],
            Experience::TO => ['nullable', 'date'],
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
