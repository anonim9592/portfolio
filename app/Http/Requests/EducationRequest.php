<?php

namespace App\Http\Requests;

use App\Interfaces\Data\Education;
use Illuminate\Foundation\Http\FormRequest;

class EducationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Education::TITLE => ['required', 'string', 'max:255'],
            Education::DESCRIPTION => ['required', 'string', 'max:1000'],
            Education::SCHOOL => ['required', 'string', 'max:255'],
            Education::SPECIALITY => ['required', 'string', 'max:255'],
            Education::FROM => ['required', 'date'],
            Education::TO => ['required', 'date'],
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
