<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChangeLocaleRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'locale' => ['required', 'string', Rule::in($this->getLocalesArray())]
        ];
    }

    private function getLocalesArray(): array
    {
        return array_keys(config('laravellocalization.supportedLocales'));
    }
}
