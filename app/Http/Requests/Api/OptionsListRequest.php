<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class OptionsListRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'keys' => ['required', 'array'],
            'keys.*' => ['required', 'string']
        ];
    }
}
