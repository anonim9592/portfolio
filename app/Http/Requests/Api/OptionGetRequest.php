<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class OptionGetRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'key' => ['required', 'max:255', 'string'],
            'value' => ['nullable', 'max:255', 'string']
        ];
    }
}
