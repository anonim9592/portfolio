<?php

namespace App\Http\Controllers;

use App\Interfaces\Data\FrontendRoutingInterface;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FrontendController extends Controller
{
    public function index(Request $request)
    {
        $component = $request->route()->parameter('component') ?? 'index';

        if (!in_array($component, FrontendRoutingInterface::AVAILABLE_COMPONENTS)) {
            throw new NotFoundHttpException();
        }

        return Inertia::render($component);
    }
}
