<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class ApiController extends Controller
{
    protected function returnResponse(
        array|string|int $result = [],
        string $message = 'successfully loaded',
        bool $success = true,
        int $code = 200
    ): JsonResponse
    {
        return response()->json([
            'success' => $success,
            'result' => $result,
            'message' => $message
        ], $code);
    }
}
