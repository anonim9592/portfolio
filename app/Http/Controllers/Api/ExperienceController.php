<?php

namespace App\Http\Controllers\Api;

use App\Service\ExperienceService;
use Illuminate\Http\JsonResponse;

class ExperienceController extends ApiController
{
    public function __construct(
        private readonly ExperienceService $experienceService,
    )
    {}

    public function list(): JsonResponse
    {
        return $this->returnResponse($this->experienceService->getListForFrontend());
    }
}
