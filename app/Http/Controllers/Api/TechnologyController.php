<?php

namespace App\Http\Controllers\Api;

use App\Service\TechnologyService;
use Illuminate\Http\JsonResponse;

class TechnologyController extends ApiController
{
    public function __construct(
        private readonly TechnologyService $technologyService,
    )
    {}

    public function list(): JsonResponse
    {
        return $this->returnResponse($this->technologyService->getListForFrontend());
    }
}
