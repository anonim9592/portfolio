<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\SubmitFormRequest;
use App\Service\ContactService;
use Illuminate\Http\JsonResponse;

class ContactController extends ApiController
{
    public function __construct(
        private readonly ContactService $contactService,
    )
    {}

    public function submit(SubmitFormRequest $request): JsonResponse
    {
        $formData = $request->validated();
        $result = $this->contactService->fillAndSave($formData);

        $message = $result ? '' : __('Error on form submitting');
        $status = $result ? 201 : 400;

        return $this->returnResponse([], $message, $result, $status);
    }
}
