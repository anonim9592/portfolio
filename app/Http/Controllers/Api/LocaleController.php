<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\ChangeLocaleRequest;
use App\Service\LocaleService;
use Illuminate\Http\JsonResponse;

class LocaleController extends ApiController
{
    public function __construct(
        private readonly LocaleService $localeService,
    )
    {}

    public function get(): JsonResponse
    {
        return $this->returnResponse($this->localeService->get());
    }

    public function getList(): JsonResponse
    {
        return $this->returnResponse($this->localeService->list());
    }

    public function change(ChangeLocaleRequest $localeRequest): JsonResponse
    {
        $locale = $localeRequest->get('locale');
        $this->localeService->set($locale);

        return $this->returnResponse($this->localeService->get());
    }
}
