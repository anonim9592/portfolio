<?php

namespace App\Http\Controllers\Api;

use App\Service\PageService;
use Illuminate\Http\JsonResponse;

class PageController extends ApiController
{
    public function __construct(
        private readonly PageService $pageService,
    )
    {}

    public function list(): JsonResponse
    {
        return $this->returnResponse($this->pageService->getAllPages());
    }
}
