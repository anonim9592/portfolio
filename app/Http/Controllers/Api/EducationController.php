<?php

namespace App\Http\Controllers\Api;

use App\Service\EducationService;
use Illuminate\Http\JsonResponse;

class EducationController extends ApiController
{
    public function __construct(
        private readonly EducationService $educationService,
    )
    {}

    public function list(): JsonResponse
    {
        return $this->returnResponse($this->educationService->getListForFrontend());
    }
}
