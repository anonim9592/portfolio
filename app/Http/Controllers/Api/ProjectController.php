<?php

namespace App\Http\Controllers\Api;

use App\Service\ProjectService;
use Illuminate\Http\JsonResponse;

class ProjectController extends ApiController
{
    public function __construct(
        private readonly ProjectService $projectService,
    )
    {}

    public function list(): JsonResponse
    {
        return $this->returnResponse($this->projectService->getListForFrontend());
    }
}
