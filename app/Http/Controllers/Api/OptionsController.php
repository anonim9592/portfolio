<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\OptionsListRequest;
use App\Service\OptionsService;
use Illuminate\Http\JsonResponse;

class OptionsController extends ApiController
{
    public function __construct(
        private readonly OptionsService $optionsService,
    ){}

    public function list(OptionsListRequest $request): JsonResponse
    {
        $keys = $request->validated('keys');

        return $this->returnResponse($this->optionsService->getOptionsArray($keys));
    }
}
