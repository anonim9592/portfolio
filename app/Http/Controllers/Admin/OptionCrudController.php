<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OptionRequest;
use App\Models\Option;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use App\Interfaces\Data\Option as OptionInterface;

/**
 * Class OptionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class OptionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    public function setup()
    {
        $this->crud->setModel(Option::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/option');
        $this->crud->setEntityNameStrings('option', 'options');
    }

    protected function setupListOperation()
    {
        $this->crud->column(OptionInterface::KEY);
        $this->crud->column(OptionInterface::VALUE);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(OptionRequest::class);

        $this->crud->addField([
            'name' => OptionInterface::KEY,
            'type' => 'text',
            'label' => "Key"
        ]);
        $this->crud->addField([
            'name' => OptionInterface::VALUE,
            'type' => 'text',
            'label' => "Value"
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
