<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ContactFormRequest;
use App\Models\ContactForm;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Interfaces\Data\ContactForm as ContactFormInterface;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;

/**
 * Class ContactFormCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ContactFormCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    public function setup()
    {
        $this->crud->setModel(ContactForm::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/contact-form');
        $this->crud->setEntityNameStrings('contact form', 'contact forms');
    }

    protected function setupListOperation()
    {
        $this->crud->column(ContactFormInterface::NAME);
        $this->crud->column(ContactFormInterface::EMAIL);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ContactFormRequest::class);

        $this->crud->addField([
            'name' => ContactFormInterface::NAME,
            'type' => 'text',
            'label' => "Name"
        ]);
        $this->crud->addField([
            'name' => ContactFormInterface::EMAIL,
            'type' => 'text',
            'label' => "Email"
        ]);
        $this->crud->addField([
            'name' => ContactFormInterface::MESSAGE,
            'type' => 'text',
            'label' => "Message"
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
