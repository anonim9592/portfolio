<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EducationRequest;
use App\Interfaces\Data\Education as EducationInterface;
use App\Models\Education;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;

/**
 * Class EducationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class EducationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->crud->setModel(Education::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/education');
        $this->crud->setEntityNameStrings('education', 'education');
    }

    protected function setupListOperation()
    {
        $this->crud->column(EducationInterface::TITLE);
        $this->crud->column(EducationInterface::DESCRIPTION);
        $this->crud->column(EducationInterface::SCHOOL);
        $this->crud->column(EducationInterface::SPECIALITY);
        $this->crud->column(EducationInterface::FROM);
        $this->crud->column(EducationInterface::TO);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(EducationRequest::class);

        $this->crud->addField([
            'name' => EducationInterface::TITLE,
            'type' => 'text',
            'label' => "Name"
        ]);
        $this->crud->addField([
            'name' => EducationInterface::DESCRIPTION,
            'type' => 'textarea',
            'label' => "Description"
        ]);
        $this->crud->addField([
            'name' => EducationInterface::SCHOOL,
            'type' => 'text',
            'label' => "School"
        ]);
        $this->crud->addField([
            'name' => EducationInterface::SPECIALITY,
            'type' => 'text',
            'label' => "Speciality"
        ]);
        $this->crud->addField([
            'name' => EducationInterface::FROM,
            'type' => 'date',
            'label' => "From"
        ]);
        $this->crud->addField([
            'name' => EducationInterface::TO,
            'type' => 'date',
            'label' => "To"
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
