<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TechnologyRequest;
use App\Models\Technology;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TechnologyCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class TechnologyCrudController extends CrudController
{
    use Operations\ListOperation;
    use Operations\CreateOperation;
    use Operations\UpdateOperation;
    use Operations\DeleteOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->crud->setModel(Technology::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/technology');
        $this->crud->setEntityNameStrings('technology', 'technologies');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->setColumns(['link', 'title']);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setValidation(TechnologyRequest::class);

        $this->crud->addField([
            'name' => 'title',
            'type' => 'text',
            'label' => "Name"
        ]);
        $this->crud->addField([
            'name' => 'link',
            'type' => 'text',
            'label' => "URL"
        ]);
        $this->crud->addField([
            'name' => 'icon',
            'type' => 'text',
            'label' => "Icon"
        ]);
        $this->crud->addField([
            'name' => 'favourite',
            'type' => 'checkbox',
            'label' => "Is Favourite"
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
