<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ExperienceRequest;
use App\Models\Experience;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use App\Interfaces\Data\Experience as ExperienceInterface;

/**
 * Class ExperienceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ExperienceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    public function setup()
    {
        $this->crud->setModel(Experience::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/experience');
        $this->crud->setEntityNameStrings('experience', 'experiences');
    }

    protected function setupListOperation()
    {
        $this->crud->column(ExperienceInterface::TITLE);
        $this->crud->column(ExperienceInterface::DESCRIPTION);
        $this->crud->column(ExperienceInterface::COMPANY);
        $this->crud->column(ExperienceInterface::POSITION);
        $this->crud->column(ExperienceInterface::FROM);
        $this->crud->column(ExperienceInterface::TO);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ExperienceRequest::class);

        $this->crud->addField([
            'name' => ExperienceInterface::TITLE,
            'type' => 'text',
            'label' => "Name"
        ]);
        $this->crud->addField([
            'name' => ExperienceInterface::DESCRIPTION,
            'type' => 'textarea',
            'label' => "Description"
        ]);
        $this->crud->addField([
            'name' => ExperienceInterface::COMPANY,
            'type' => 'text',
            'label' => "Company"
        ]);
        $this->crud->addField([
            'name' => ExperienceInterface::POSITION,
            'type' => 'text',
            'label' => "Position"
        ]);
        $this->crud->addField([
            'name' => ExperienceInterface::FROM,
            'type' => 'date',
            'label' => "From"
        ]);
        $this->crud->addField([
            'name' => ExperienceInterface::TO,
            'type' => 'date',
            'label' => "To"
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
