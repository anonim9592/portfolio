<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProjectRequest;
use App\Models\Project;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use App\Interfaces\Data\Project as ProjectInterface;

/**
 * Class ProjectCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ProjectCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    public function setup()
    {
        $this->crud->setModel(Project::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/project');
        $this->crud->setEntityNameStrings('project', 'projects');
    }

    protected function setupListOperation()
    {
        $this->crud->column(ProjectInterface::ID);
        $this->crud->column(ProjectInterface::TITLE);
        $this->crud->column(ProjectInterface::LINK);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ProjectRequest::class);

        $this->crud->field(ProjectInterface::TITLE);
        $this->crud->field(ProjectInterface::DESCRIPTION)->type('text');
        $this->crud->field(ProjectInterface::LINK);
        $this->crud->field(ProjectInterface::IMAGE)->type('upload');
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
