<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PageRequest;
use App\Models\Page;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use App\Interfaces\Data\Page as PageInterface;

/**
 * Class PageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    public function setup()
    {
        $this->crud->setModel(Page::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/page');
        $this->crud->setEntityNameStrings('page', 'pages');
    }

    protected function setupListOperation()
    {
        $this->crud->column(PageInterface::ID);
        $this->crud->column(PageInterface::SLUG);
        $this->crud->column(PageInterface::TITLE);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PageRequest::class);

        $this->crud->addField([
            'name' => PageInterface::TITLE,
            'type' => 'text',
            'label' => "Name"
        ]);
        $this->crud->addField([
            'name' => PageInterface::SLUG,
            'type' => 'text',
            'label' => "URL (slug)"
        ]);
        $this->crud->addField([
            'name' => PageInterface::CONTENT,
            'type' => 'textarea',
            'label' => "Page content"
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
