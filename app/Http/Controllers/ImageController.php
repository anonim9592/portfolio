<?php

namespace App\Http\Controllers;

use League\Glide\Server;

class ImageController extends Controller
{

    public function __invoke(Server $server, string $path): void
    {
        $server->outputImage($path, request()->all());
    }
}
