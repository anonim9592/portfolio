<?php

namespace App\Listeners;

use App\Events\ContactFormCreated;
use App\Service\TelegramService;

class SendTelegramNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        private readonly TelegramService $telegramService
    )
    {}

    /**
     * Handle the event.
     *
     * @param  \App\Events\ContactFormCreated  $event
     * @return void
     */
    public function handle(ContactFormCreated $event)
    {
        $formData = $event->contactFormDTO;

        $this->telegramService->sendMessageToBot($formData);
    }
}
