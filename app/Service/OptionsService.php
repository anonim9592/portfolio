<?php

namespace App\Service;

use App\Query\OptionQuery;
use App\Models\Option;
use JetBrains\PhpStorm\ArrayShape;

class OptionsService extends BaseService
{
    public function __construct(
        private readonly OptionQuery $optionQuery,
        private readonly CacheService $cacheService,
    ){}

    public function getOptionsArray(array $keys): array
    {
        return $this->cacheService->getOrSet(
            implode('_', $keys),
            function () use ($keys) {
                foreach ($keys as $key) {
                    $this->createIfNotExist($key, $key);
                }
                $options = $this->optionQuery->getMany($keys);
                $result = [];

                foreach ($options as $option) {
                    $result[] = $this->serialize($option);
                }

                return $result;
            },
            86400
        );
    }

    private function createIfNotExist(string $key, string $default): void
    {
        if (!$this->optionQuery->exists($key)) {
            $this->optionQuery->set($key, $default);
        }
    }

    #[ArrayShape(['key' => "string", 'value' => "string"])]
    private function serialize(Option $option): array
    {
        return [
            'key' => $option->key,
            'value' => $option->value
        ];
    }
}
