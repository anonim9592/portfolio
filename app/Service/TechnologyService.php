<?php

namespace App\Service;

use App\Models\Technology;
use App\Query\TechnologyQuery;

class TechnologyService extends BaseService
{
    public function __construct(
        private readonly TechnologyQuery $technologyQuery,
        private readonly CacheService $cacheService,
    ){}

    public function getListForFrontend(): array
    {
        return $this->cacheService->getOrSet(
            Technology::LIST_CACHE_KEY,
            function () {
                return $this->technologyQuery->getList()->toArray();
            }
        );
    }
}
