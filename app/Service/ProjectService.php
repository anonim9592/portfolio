<?php

namespace App\Service;

use App\Models\Project;
use App\Query\ProjectQuery;
use Illuminate\Support\Collection;

class ProjectService extends BaseService
{
    public function __construct(
        private readonly ProjectQuery $projectQuery,
        private readonly CacheService $cacheService,
    ){}

    public function getListForFrontend(): array
    {
        return $this->cacheService->getOrSet(
            Project::LIST_CACHE_KEY,
            function () {
                $projectList = $this->projectQuery->getList();

                return $this->serialize($projectList);
            }
        );
    }

    private function serialize(Collection $collection): array
    {
        $result = [];
        foreach ($collection as $item) {
            $result[] = [
                'title' => $item->title,
                'description' => $item->description,
                'link' => $item->link,
                'image' => route('image.resize', $item->image),
            ];
        }

        return $result;
    }
}
