<?php

namespace App\Service;

use App\Models\Education;
use App\Query\EducationQuery;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class EducationService extends BaseService
{
    public function __construct(
        private readonly EducationQuery $educationQuery,
        private readonly CacheService $cacheService,
    ){}

    public function getListForFrontend(): array
    {
        return $this->cacheService->getOrSet(
            Education::LIST_CACHE_KEY,
            function () {
                $educationList = $this->educationQuery->getList();
                return $this->serialize($educationList);
            }
        );
    }

    private function serialize(Collection $collection): array
    {
        $result = [];
        foreach ($collection as $experience) {
            $result[] = [
                'title' => $experience->title,
                'description' => $experience->description,
                'school' => $experience->school,
                'speciality' => $experience->speciality,
                'from' => Carbon::parse($experience->from)->format('Y'),
                'to' => $experience->to ? Carbon::parse($experience->to)->format('Y') : __('now'),
            ];
        }

        return $result;
    }
}
