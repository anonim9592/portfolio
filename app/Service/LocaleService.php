<?php

namespace App\Service;

class LocaleService extends BaseService
{
    public function get(): string
    {
        return \App::getLocale();
    }

    public function set(string $locale): void
    {
        \App::setLocale($locale);
    }

    public function list(): array
    {
        return config('laravellocalization.supportedLocales');
    }

}
