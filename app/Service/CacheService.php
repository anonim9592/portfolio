<?php

namespace App\Service;

use Illuminate\Cache\Repository as CacheRepository;

class CacheService
{
    public function __construct(
        private readonly CacheRepository $cacheRepository,
    ){}


    public function getOrSet(string $cacheKey, \Closure $value, int $ttl = null): mixed
    {
        if (!$this->cacheRepository->has($this->getLocalizedCacheKey($cacheKey))) {
            $this->set($cacheKey, call_user_func($value), $ttl);
        }

        return $this->get($cacheKey);
    }

    public function get(string $cacheKey): mixed
    {
        return $this->cacheRepository->get($this->getLocalizedCacheKey($cacheKey));
    }

    public function set(string $cacheKey, mixed $value, int $ttl = null): bool
    {
        return $this->cacheRepository->set($this->getLocalizedCacheKey($cacheKey), $value, $ttl);
    }

    private function getLocalizedCacheKey(string $cacheKey): string
    {
        return \App::getLocale() . '_' . $cacheKey;
    }
}
