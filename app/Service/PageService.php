<?php

namespace App\Service;

use App\Models\Page;
use App\Query\PageQuery;
use Illuminate\Support\Collection;

class PageService extends BaseService
{
    public function __construct(
        private readonly PageQuery $pageQuery,
        private readonly CacheService $cacheService,
    ){}

    public function getAllPages(): array
    {
        return $this->cacheService->getOrSet(
            Page::LIST_CACHE_KEY,
            function () {
                $pages = $this->pageQuery->getForRouterPages();
                return $this->serialize($pages);
            }
        );
    }

    public function getSingle(string $slug): array
    {
        $page = $this->pageQuery->getBySlug($slug);

        return $page->toArray();
    }

    private function serialize(Collection $pages): array
    {
        $result = [];
        foreach ($pages as $page) {
            $result[] = [
                'title' => $page->title,
                'route' => route('frontend.index', ['component' => $page->slug]),
            ];
        }

        return $result;
    }
}
