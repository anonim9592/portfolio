<?php

namespace App\Service;

use App\Models\Experience;
use App\Query\ExperienceQuery;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ExperienceService extends BaseService
{
    public function __construct(
        private readonly ExperienceQuery $experienceQuery,
        private readonly CacheService $cacheService,
    ){}


    public function getListForFrontend(): array
    {
        return $this->cacheService->getOrSet(
            Experience::LIST_CACHE_KEY,
            function () {
                $experiences = $this->experienceQuery->getList();
                return $this->serialize($experiences);
            }
        );
    }

    private function serialize(Collection $collection)
    {
        $result = [];
        foreach ($collection as $experience) {
            $result[] = [
                'title' => $experience->title,
                'description' => $experience->description,
                'company' => $experience->company,
                'position' => $experience->position,
                'from' => Carbon::parse($experience->from)->format('M Y'),
                'to' => $experience->to ? Carbon::parse($experience->to)->format('M Y') : __('now'),
            ];
        }

        return $result;
    }
}
