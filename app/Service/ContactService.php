<?php

namespace App\Service;

use App\DTO\ContactFormDTO;
use App\Events\ContactFormCreated;
use App\Query\ContactFormQuery;

class ContactService extends BaseService
{

    public function __construct(
        private readonly ContactFormQuery $contactFormQuery,
    )
    {}

    public function fillAndSave(array $formData): bool
    {
        try {
            $formDTO = new ContactFormDTO();
            $formDTO->fillFromArray($formData);
            $this->contactFormQuery->saveToDb($formDTO);
            ContactFormCreated::dispatch($formDTO);
        } catch (\Exception $exception) {
            return false;
        }

        return true;

    }
}
