<?php

namespace App\Service;

use App\DTO\ContactFormDTO;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\Objects\Message as MessageObject;

class TelegramService extends BaseService
{
    public function __construct(
        private readonly Api $telegramApi,
    )
    {}

    /**
     * @throws TelegramSDKException
     */
    public function sendMessageToBot(ContactFormDTO $formDTO): MessageObject
    {
        return $this->telegramApi->sendMessage($this->getMessageParams($formDTO));
    }

    private function getMessageParams(ContactFormDTO $formDTO): array
    {
        return [
            'chat_id' => config('telegram.base_chat_id'),
            'text' => $this->getMessageText($formDTO)
        ];
    }

    private function getMessageText(ContactFormDTO $formDTO): string
    {
        return 'New message from portfolio site:' . PHP_EOL
            . $formDTO->getName() . PHP_EOL
            . $formDTO->getEmail() . PHP_EOL
            . $formDTO->getMessage();
    }
}
