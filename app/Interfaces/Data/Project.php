<?php

namespace App\Interfaces\Data;

interface Project
{
    public const LIST_CACHE_KEY = 'projects_list';

    public const ID = 'id';
    public const TITLE = 'title';
    public const DESCRIPTION = 'description';
    public const LINK = 'link';
    public const IMAGE = 'image';
    public const SORT_ORDER = 'sort_order';
}
