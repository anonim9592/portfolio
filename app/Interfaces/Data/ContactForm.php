<?php

namespace App\Interfaces\Data;

interface ContactForm
{
    public const NAME = 'name';
    public const EMAIL = 'email';
    public const MESSAGE = 'message';
}
