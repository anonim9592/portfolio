<?php

namespace App\Interfaces\Data;

interface Option
{
    public const KEY = 'key';
    public const VALUE = 'value';
}
