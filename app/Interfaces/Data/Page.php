<?php

namespace App\Interfaces\Data;

interface Page
{
    public const LIST_CACHE_KEY = 'pages_list';

    public const ID = 'id';
    public const TITLE = 'title';
    public const CONTENT = 'content';
    public const SLUG = 'slug';
}
