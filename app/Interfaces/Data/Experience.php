<?php

namespace App\Interfaces\Data;

interface Experience
{
    public const LIST_CACHE_KEY = 'experience_list';

    public const TITLE = 'title';
    public const DESCRIPTION = 'description';
    public const COMPANY = 'company';
    public const POSITION = 'position';
    public const FROM = 'from';
    public const TO = 'to';
}
