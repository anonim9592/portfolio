<?php

namespace App\Interfaces\Data;

interface Education
{
    public const LIST_CACHE_KEY = 'education_list';

    public const TITLE = 'title';
    public const DESCRIPTION = 'description';
    public const SCHOOL = 'school';
    public const SPECIALITY = 'speciality';
    public const FROM = 'from';
    public const TO = 'to';
}
