<?php

namespace App\Interfaces\Data;

interface FrontendRoutingInterface
{
    public const AVAILABLE_COMPONENTS = ['index', 'resume', 'portfolio', 'contact'];
}
