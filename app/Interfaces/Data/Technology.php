<?php

namespace App\Interfaces\Data;

interface Technology
{
    public const LIST_CACHE_KEY = 'technologies_list';

    public const ID = 'id';
    public const TITLE = 'title';
    public const LINK = 'link';
    public const ICON = 'icon';
    public const SORT_ORDER = 'sort_order';
    public const FAVOURITE = 'favourite';
}
