<?php

namespace App\Models;

use App\Interfaces\Data\Education as EducationInterface;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Education extends Model implements EducationInterface
{
    use CrudTrait;
    use HasFactory, HasTranslations;

    public array $translatable = [
        self::TITLE,
        self::DESCRIPTION,
        self::SCHOOL,
        self::SPECIALITY,
    ];

    public $fillable = [
        self::TITLE,
        self::DESCRIPTION,
        self::SCHOOL,
        self::SPECIALITY,
        self::FROM,
        self::TO,
    ];

    public $casts = [
        self::FROM,
        self::TO,
    ];
}
