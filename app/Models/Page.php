<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Data\Page as PageInterface;

class Page extends Model implements PageInterface
{
    use CrudTrait;
    use HasFactory,
        HasTranslations;

    public $casts = [
        self::SLUG => 'string',
    ];

    public $fillable = [self::SLUG, self::TITLE, self::CONTENT];

    public array $translatable = [self::TITLE, self::CONTENT];
}
