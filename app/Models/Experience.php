<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Data\Experience as ExperienceInterface;

class Experience extends Model implements ExperienceInterface
{
    use CrudTrait;
    use HasFactory, HasTranslations;

    public array $translatable = [
        self::TITLE,
        self::DESCRIPTION,
        self::COMPANY,
        self::POSITION,
    ];

    public $fillable = [
        self::TITLE,
        self::DESCRIPTION,
        self::COMPANY,
        self::POSITION,
        self::FROM,
        self::TO,
    ];

    public $casts = [
        self::FROM,
        self::TO,
    ];
}
