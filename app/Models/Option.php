<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Jenssegers\Mongodb\Eloquent\Model as MongoModel;
use App\Interfaces\Data\Option as OptionInterface;

class Option extends MongoModel implements OptionInterface
{
    use CrudTrait;
    use HasTranslations;

    public $timestamps = false;
    protected $connection = 'mongodb';
    protected $collection = 'tratslations';

    public array $translatable = [
        OptionInterface::VALUE,
    ];

    protected $fillable = [
        OptionInterface::KEY,
        OptionInterface::VALUE,
    ];
}
