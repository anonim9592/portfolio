<?php

namespace App\Models;

use App\Interfaces\Data\ContactForm as ContactFormInterface;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactForm extends Model implements ContactFormInterface
{
    use CrudTrait;
    use HasFactory;

    public $fillable = [
        self::NAME,
        self::EMAIL,
        self::MESSAGE
    ];
}
