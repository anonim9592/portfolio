<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Data\Project as ProjectInterface;

class Project extends Model implements ProjectInterface
{
    use CrudTrait;
    use HasFactory, HasTranslations;

    public array $translatable = [
        self::DESCRIPTION,
    ];

    public $fillable = [
        self::TITLE,
        self::DESCRIPTION,
        self::LINK,
        self::IMAGE,
    ];
}
