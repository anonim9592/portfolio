<?php

namespace App\Models;

use App\Interfaces\Data\Technology as TechnologyInterface;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasTranslations;

class Technology extends Model implements TechnologyInterface
{
    use CrudTrait,
        HasFactory,
        HasTranslations;

    public $casts = [
        self::LINK => 'string',
        self::ICON => 'string',
        self::FAVOURITE => 'boolean'
    ];

    protected $fillable = [self::TITLE, self::LINK, self::ICON, self::SORT_ORDER, self::FAVOURITE];

    public array $translatable = [self::TITLE];

}
