<?php

namespace App\Providers;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use League\Glide\Server;
use League\Glide\ServerFactory;

class GlideServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Server::class, function ($app) {
            $filesystem = $app->make(Filesystem::class);

            return ServerFactory::create([
                'source' => $filesystem->getDriver(),
                'cache' => $filesystem->getDriver(),
                'source_path_prefix' => 'public/images',
                'cache_path_prefix' => 'public/images/.cache',
                'base_url' => 'img',
            ]);
        });
    }

    public function boot()
    {
        //
    }
}
