<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ImageController;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([], function () {
    Route::get('/', [FrontendController::class, 'index'], '')->name('frontend.index.home');
    Route::get('{component}', [FrontendController::class, 'index'])->name('frontend.index');
});
Route::get('/img/{path}', [ImageController::class, '__invoke'])
    ->where('path', '.*')
    ->name('image.resize');
