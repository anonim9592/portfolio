<?php

use App\Http\Controllers\Api\ContactController;
use App\Http\Controllers\Api\EducationController;
use App\Http\Controllers\Api\ExperienceController;
use App\Http\Controllers\Api\LocaleController;
use App\Http\Controllers\Api\OptionsController;
use App\Http\Controllers\Api\PageController;
use App\Http\Controllers\Api\ProjectController;
use App\Http\Controllers\Api\TechnologyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')
    ->group(function () {
        Route::post('pages', [PageController::class, 'list'])->name('page.list');
        Route::post('technologies', [TechnologyController::class, 'list'])->name('technology.list');
        Route::post('experience', [ExperienceController::class, 'list'])->name('experience.list');
        Route::post('education', [EducationController::class, 'list'])->name('education.list');
        Route::post('translations', [OptionsController::class, 'list'])->name('translations.list');
        Route::post('projects', [ProjectController::class, 'list'])->name('project.list');
        Route::post('contact', [ContactController::class, 'submit'])->name('contact.submit');
        Route::post('locale/get', [LocaleController::class, 'get'])->name('locale.get');
        Route::post('locale/list', [LocaleController::class, 'getList'])->name('locale.list');
        Route::post('locale/set', [LocaleController::class, 'change'])->name('locale.set');
    }
);
